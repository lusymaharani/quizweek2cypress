# Cypress Testing Repository for Login and Appointment Booking

This repository contains automated testing scripts for the login and appointment booking functionalities of the website [https://katalon-demo-cura.herokuapp.com/](https://katalon-demo-cura.herokuapp.com/) using Cypress.

## Table of Contents

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)

## Introduction

This repository is dedicated to automating the testing of the login and appointment booking features of the [Katalon Demo Cura](https://katalon-demo-cura.herokuapp.com/) website using the Cypress testing framework. 

## Prerequisites

Before you can run the test scripts in this repository, you need to have the following prerequisites installed:

- [Node.js](https://nodejs.org/): Ensure you have Node.js installed on your machine.

```bash
# Install dependencies project
cd QuizWeek2Cypress
npm install
