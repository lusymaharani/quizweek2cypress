class LoginPage {
    visit() {
      cy.visit('https://katalon-demo-cura.herokuapp.com/');
      cy.get('#menu-toggle').click()
      cy.get('.sidebar-nav > :nth-child(4) > a').click()
    }
  
    fillUsername(username) {
      cy.get('#txt-username').type(username);
    }
  
    fillPassword(password) {
      cy.get('#txt-password').type(password);
    }
  
    clickLoginButton() {
      cy.get('#btn-login').click();
    }
}
  
  export default new LoginPage;
  