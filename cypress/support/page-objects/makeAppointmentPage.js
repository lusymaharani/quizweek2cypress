
//       cy.get(`input[name='programs'][value='${data.program}']`).check();     
  
class MakeAppointmentPage {
    visitMakeAppointmentPage() {
        cy.visit('https://katalon-demo-cura.herokuapp.com/')
        cy.get('#btn-make-appointment').click()
    }
  
    selectFacility(facility) {
        cy.get('#combo_facility').select(facility)
    }

    checklistReadmission() {
        cy.get('#chk_hospotal_readmission').click()
    }
  
    selectProgram(program) {
        cy.get(`input[name='programs'][value='${program}']`).check()
    }

    selectVisitDate(date) {
        cy.get('#txt_visit_date').type(date)
    }
  
    fillComment(comment) {
        cy.get('#txt_comment').type(comment, { force: true })
    }
  
    clickBookAppointmentButton() {
        cy.get('#btn-book-appointment').click()
    }
  
    makeAppointment(facility, program, date, comment) {
      this.visitMakeAppointmentPage()
      this.selectFacility(facility)
      this.selectProgram(program)
      this.selectVisitDate(date)
      this.fillComment(comment)
      this.clickBookAppointmentButton()
    }

    makeReadmission(facility, program, date, comment) {
        this.visitMakeAppointmentPage()
        this.selectFacility(facility)
        this.checklistReadmission()
        this.selectProgram(program)
        this.selectVisitDate(date)
        this.fillComment(comment)
        this.clickBookAppointmentButton()
      }
}
  
  export default new MakeAppointmentPage()
  