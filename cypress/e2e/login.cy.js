import LoginPage from '../support/page-objects/loginPage';

describe('Login Test', () => {
    it('Unsuccessfully login', () => {
      // Tindakan untuk kredensial yang salah
      LoginPage.visit();
      LoginPage.fillUsername('Lusy');
      LoginPage.fillPassword('Inikatasandi');
      LoginPage.clickLoginButton();
  
      // Verifikasi bahwa URL saat ini adalah halaman login (karena login gagal)
      cy.url().should('eq', 'https://katalon-demo-cura.herokuapp.com/profile.php#login');
  
      // Verifikasi pesan error hanya untuk kredensial yang salah
      cy.get('.text-danger').should('be.visible');
      cy.get('.text-danger').should('contain', 'Login failed! Please ensure the username and password are valid.');
    });
  
    it('Successfully login with valid credentials', () => {
      // Tindakan untuk kredensial yang benar
      LoginPage.visit();
      LoginPage.fillUsername('John Doe');
      LoginPage.fillPassword('ThisIsNotAPassword');
      LoginPage.clickLoginButton();
  
      // Verifikasi bahwa URL saat ini adalah halaman appointment (karena login berhasil)
      cy.url().should('eq', 'https://katalon-demo-cura.herokuapp.com/#appointment');
    });
  });
  