import LoginPage from '../support/page-objects/loginPage';
import MakeAppointmentPage from '../support/page-objects/makeAppointmentPage';

describe('Make Appointments', () => {

  beforeEach(() => {
    LoginPage.visit();
    LoginPage.fillUsername('John Doe');
    LoginPage.fillPassword('ThisIsNotAPassword');
    LoginPage.clickLoginButton();

    cy.url().should('eq', 'https://katalon-demo-cura.herokuapp.com/#appointment');
  });

  it('Make an appointment in Hongkong with Medicare program', () => {
    MakeAppointmentPage.makeAppointment('Hongkong CURA Healthcare Center', 'Medicare', '2023-09-09', 'Test appointment in Hongkong with Medicare program')
    cy.wait(5000)
    cy.get('.text-center > .btn').click()
  })

  it('Make an appointment in Tokyo with Medicaid program', () => {
    MakeAppointmentPage.makeAppointment('Tokyo CURA Healthcare Center', 'Medicaid', '2023-09-10', 'Test appointment in Tokyo with Medicaid program')
    cy.wait(5000)
    cy.get('.text-center > .btn').click()
  })

  it('Apply for readmission', () => {
    MakeAppointmentPage.makeReadmission('Seoul CURA Healthcare Center', 'None', '2023-09-10', 'Test readmission')
    cy.wait(5000)
    cy.get('.text-center > .btn').click()
  })
  
});
